package com.summit.whms.common.dto;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Map;

@Getter
public class PaginationDto {
    private int totalCount;
    private int rowSize;
    private int prev;
    private int next;
    private int totalPageSize;
    private ArrayList<Map> pageNumbers;

    public PaginationDto(int totalCount , int rowSize, int prev, int next, int totalPageSize, ArrayList<Map> pageNumbers) {
        this.totalCount = totalCount;
        this.rowSize = rowSize;
        this.prev = prev;
        this.next = next;
        this.totalPageSize = totalPageSize;
        this.pageNumbers = pageNumbers;
    }


}
