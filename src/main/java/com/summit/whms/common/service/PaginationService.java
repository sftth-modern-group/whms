package com.summit.whms.common.service;

import com.summit.whms.common.dto.PaginationDto;

public interface PaginationService {
    public PaginationDto paginationInfo(int totalCount, int pagenum, int rowSize);
}
