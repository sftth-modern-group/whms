package com.summit.whms.common.service;

import com.summit.whms.common.dto.PaginationDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class PaginationServiceImpl implements PaginationService{
    private static final int DEFAULT_PAGE_SIZE = 10;
    private static final int DEFAULT_NUMBER = 1;

    @Override
    public PaginationDto paginationInfo(int totalCount, int pagenum, int rowSize) {
        int prev = pagenum - 1;
        int next = pagenum + 1;

        int totalPageSize = totalCount % rowSize > 0 ? (totalCount / rowSize)+1 : totalCount / rowSize ;

        int pageSizePerPage = totalPageSize > DEFAULT_PAGE_SIZE ? DEFAULT_PAGE_SIZE : totalPageSize;

        int opset = (pagenum-1) / pageSizePerPage * pageSizePerPage;
        int startPageNum = opset + 1;
        int lastPageNum = (opset + pageSizePerPage) > totalPageSize ? totalPageSize:(opset + pageSizePerPage);

        ArrayList<Map> pageNumberList = new ArrayList<Map>();

        for(int i = startPageNum ; i <= lastPageNum ; i++) {
            Map pageNumberMap = new LinkedHashMap();
            pageNumberMap.put("pageNumber", i);
            pageNumberList.add(pageNumberMap);
        }

        PaginationDto paginationDto = new PaginationDto(totalCount, rowSize, prev, next, totalPageSize, pageNumberList);

        return paginationDto;
    }
}
