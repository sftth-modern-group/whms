package com.summit.whms.openapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.summit.whms.config.auth.LoginUser;
import com.summit.whms.config.auth.dto.SessionUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Controller
public class OpenApiController {
    private static  final Logger LOGGER = LoggerFactory.getLogger(OpenApiController.class);

    RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/openapi/subway-stats")
    public String cardSubwayStatsNewView(Model model, @LoginUser SessionUser user) throws JsonProcessingException {
        LOGGER.info("/openapi/subway-stats is called ");

        if(user != null) {
            model.addAttribute("userName", user.getName());
            model.addAttribute("picture", user.getPicture());
        }

        return "openapi/statsnew";
    }

    @PostMapping("/openapi/subway-stats")
    public String cardSubwayStatsNew(Model model, @LoginUser SessionUser user, @ModelAttribute RowDto rowDto) throws JsonProcessingException {
        LOGGER.info("POST /openapi/subway-stats is called ");
        LOGGER.info("USE DT " + rowDto.getUse_dt());

        if(user != null) {
            model.addAttribute("userName", user.getName());
            model.addAttribute("picture", user.getPicture());
        }

        String url = "http://openapi.seoul.go.kr:8088/484b66624673667433394d75714a4a/json/CardSubwayStatsNew/1/50/"+rowDto.getUse_dt();

        ResponseEntity<Map> resultMap = restTemplate.getForEntity(url, Map.class);

        LinkedHashMap statsNew = (LinkedHashMap) Objects.requireNonNull(resultMap.getBody()).get("CardSubwayStatsNew");
        ArrayList<Map> rows = (ArrayList<Map>) statsNew.get("row");


        ObjectMapper mapper = new ObjectMapper();
        List<RowDto> rowDtoList = new ArrayList<RowDto>();


        String rowsString = mapper.writeValueAsString(rows);

        for(Map map : rows) {
            LinkedHashMap rowMap = (LinkedHashMap) map;
            rowDtoList.add(new RowDto(
                    (String) rowMap.get("USE_DT"),
                    (String) rowMap.get("LINE_NUM"),
                    (String) rowMap.get("SUB_STA_NM"),
                    (Double) rowMap.get("RIDE_PASGR_NUM"),
                    (Double) rowMap.get("ALIGHT_PASGR_NUM"),
                    (String) rowMap.get("WORK_DT")));
        }

        model.addAttribute("posts", rowDtoList);

        return "openapi/statsnew";
    }
}
