package com.summit.whms.openapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.summit.whms.common.dto.PaginationDto;
import com.summit.whms.common.service.PaginationService;
import com.summit.whms.config.auth.LoginUser;
import com.summit.whms.config.auth.dto.SessionUser;
import com.summit.whms.openapi.dto.StatsNewDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RestController
public class OpenApiRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiRestController.class);

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    PaginationService paginationService;

    @GetMapping("/openapi/subway-stats/{usedt}/{pagenum}/{rowSize}")
    public StatsNewDto cardSubwayStatsNewView(Model model,
                                              @LoginUser SessionUser user,
                                              @PathVariable String usedt,
                                              @PathVariable int pagenum,
                                              @PathVariable int rowSize) throws JsonProcessingException {
        LOGGER.info("RestController/openapi/subway-stats/" + usedt + "/" + pagenum + " is called ");
        if(user != null) {
            model.addAttribute("userName", user.getName());
            model.addAttribute("picture", user.getPicture());
        }

        int startno = 1 + rowSize * (pagenum -1);
        int endno = rowSize + rowSize * (pagenum-1);

        String url = "http://openapi.seoul.go.kr:8088/484b66624673667433394d75714a4a/json/CardSubwayStatsNew/" +
                startno + "/" + endno + "/" + usedt;

        ResponseEntity<Map> resultMap = restTemplate.getForEntity(url, Map.class);

        LinkedHashMap statsNew = (LinkedHashMap) Objects.requireNonNull(resultMap.getBody()).get("CardSubwayStatsNew");

        ArrayList<Map> rows = (ArrayList<Map>) statsNew.get("row");


        ObjectMapper mapper = new ObjectMapper();
        List<RowDto> rowDtoList = new ArrayList<RowDto>();

        String rowsString = mapper.writeValueAsString(rows);

        for(Map map : rows) {
            LinkedHashMap rowMap = (LinkedHashMap) map;
            rowDtoList.add(new RowDto(
                    (String) rowMap.get("USE_DT"),
                    (String) rowMap.get("LINE_NUM"),
                    (String) rowMap.get("SUB_STA_NM"),
                    (Double) rowMap.get("RIDE_PASGR_NUM"),
                    (Double) rowMap.get("ALIGHT_PASGR_NUM"),
                    (String) rowMap.get("WORK_DT")));
        }

        int totalCount = (Integer) statsNew.get("list_total_count");

        PaginationDto paginationDto = paginationService.paginationInfo(totalCount,pagenum,rowSize);
        StatsNewDto statsNewDto = new StatsNewDto(rowDtoList,paginationDto);

        return statsNewDto;
    }
}
