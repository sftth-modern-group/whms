package com.summit.whms.openapi;

import lombok.Getter;

@Getter
public class RowDto {
    private String use_dt;
    private String line_num;
    private String sub_sta_nm;
    private Double ride_pasgr_num;
    private Double alight_pasgr_num;
    private String work_dt;

    public RowDto(String use_dt,
                  String line_num,
                  String sub_sta_nm,
                  Double ride_pasgr_num,
                  Double alight_pasgr_num,
                  String work_dt) {
        this.use_dt = use_dt;
        this.line_num = line_num;
        this.sub_sta_nm = sub_sta_nm;
        this.ride_pasgr_num = ride_pasgr_num;
        this.alight_pasgr_num = alight_pasgr_num;
        this. work_dt = work_dt;
    }
}
