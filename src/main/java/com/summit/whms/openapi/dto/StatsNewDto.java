package com.summit.whms.openapi.dto;

import com.summit.whms.common.dto.PaginationDto;
import com.summit.whms.openapi.RowDto;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
public class StatsNewDto {
    private PaginationDto paginationDto;
    private List<RowDto> rowDtoList;

    public StatsNewDto(List<RowDto> rowDtoList, PaginationDto paginationDto) {
        this.paginationDto = paginationDto;
        this.rowDtoList = rowDtoList;
    }
}
