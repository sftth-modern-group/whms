package com.summit.whms.openapi;

import lombok.Data;

@Data
public class Person {
    private String name;
    private Integer age;
}
