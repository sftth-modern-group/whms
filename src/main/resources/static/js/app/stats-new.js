var common = {
    pagination: function (pagenum,data) {
        var totalCount = data.paginationDto.totalCount;
        var prev = data.paginationDto.prev;
        var next = data.paginationDto.next;
        var totalPageSize = data.paginationDto.totalPageSize;
        var rowSize = data.paginationDto.rowSize;
        $('#totalCount').text(" of " +totalCount);

        var paginationDto = data.paginationDto;
        var numberJSON = JSON.stringify(paginationDto);

        fetch('/temp/pagnation.mustache')
            .then((response) => response.text())
            .then((template) => {
                var rendered = Mustache.render(template, JSON.parse(numberJSON));
                $("#pagination").html(rendered);
                $('#no'+pagenum).parent().addClass("active");
                if(prev === 0) {
                    $('#prev').parent().addClass("disabled");
                }
                if(next > totalPageSize) {
                    $('#next').parent().addClass("disabled");
                }
            });
    }
}

var main = {
    init: function () {
        var _this = this;

        $('#btn-stats-new').on('click', function(){
            console.log("search");
           _this.search(1);
        });
        $(document).on("click", 'a[name="pagenum"]', function () {
            // var pageno = this.text;
            var pageno = this.getAttribute('data-dt-idx');
            console.log("page number " + pageno);
            _this.search(pageno);
        })
    },
    search: function (pagenum) {
        var datePicker = $('#datePicker').val();
        var usedt = datePicker.replace(/-/g,"");
        var rowsize = $('#rowSize').val();

        console.log("usedt :" + usedt);
        console.log("pagenum :" + pagenum);
        console.log("rowsize :" + rowsize);

        $.ajax({
            type: 'GET',
            url: '/openapi/subway-stats/'+usedt+'/'+pagenum+'/'+rowsize,
            // dataType: 'json',
            contentType:'application/json; charset=utf-8',
            //data: JSON.stringify(data)
        }).done(function (data) {
            console.log('조회 완료');
            var rows = data.rowDtoList;
            var result = '{"posts":' + JSON.stringify(rows) + "}";

            fetch('/temp/template.mustache')
                .then((response) => response.text())
                .then((template) => {
                    var rendered = Mustache.render(template, JSON.parse(result));
                    $("#tbody").html(rendered);
                });
            common.pagination(pagenum,data);
        }).fail(function (e) {
            alert(JSON.stringify(e));
        });
    }
};

main.init();

